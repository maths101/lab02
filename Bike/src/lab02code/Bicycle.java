/* Grigor Mihaylov 1937997 */

package lab02code;

public class Bicycle {
	private String manufacteur;
	private int numberGears;
	private double maxSpeed;

	public Bicycle(String manufacteur, int numberGears, double maxSpeed) {
		if (numberGears < 0) {
			throw new IllegalArgumentException("cant be negative: " + numberGears);
		}
		

		if (maxSpeed < 0) {
			throw new IllegalArgumentException("cant be negative: " + maxSpeed);
		}
		
		this.manufacteur = manufacteur;
		this.maxSpeed = maxSpeed;
		this.numberGears = numberGears;
	}

	public String getManufacteur() {
		return this.manufacteur;

	}

	public double getMaxSpeed() {
		return this.maxSpeed;

	}

	public int getNumberGears() {
		return this.numberGears;

	}

	public String toString() {
		String s = "";
		s += "Manufacteur: " + manufacteur;
		s += ", Number of gears: " + numberGears;
		s += ", Max Speed: " + maxSpeed;
		return s;
	}
}
