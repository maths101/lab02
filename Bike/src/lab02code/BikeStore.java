/* Grigor Mihaylov 1937997 */
package lab02code;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("Specialized", 6, 200.0);
		bikes[1] = new Bicycle("Standard", 7, 300.0);
		bikes[2] = new Bicycle("TOP SECRET", 8, 400.0);
		bikes[3] = new Bicycle("DATA CORRUPTED (jk LOL)", 5, 150.0);
		
		for(int i = 0 ; i <bikes.length; i++) {
			System.out.println(bikes[i]);
			
		}
	}
}
